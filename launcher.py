#! /usr/bin/env python3
from client import UI
import socket
import os
import json
from os import path
from common import *

# init ui first
ui = UI()

# define send_file function (it will be triggered from ui)
def send_file(file_name):
    # use update_progress to update progress bar of a given file
    ui.update_progress(file_name, 0)

    address = ui.get_address()

    # opening UDP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    
    try:
        # first package is for metainformation. TODO add CRC
        size = os.path.getsize(file_name)
        sha256 = sha256_checksum(file_name)
        meta = json.dumps({'size':      size,
                           'file_name': path.basename(file_name),
                           'sha256':sha256})
        print('sending file. meta:', meta)
        sock.sendto(meta.encode(), address)

        sent = 0
        with open(file_name, 'rb') as file:
            while sent < size:
                buffer = file.read(BUFFER_SIZE)
                sock.sendto(buffer, address)
                print('sent {0} bytes'.format(len(buffer)))
                sent += len(buffer)
                ui.update_progress(file_name, 100*sent//size)
        print('total sent {0}'.format(sent))
    except (OSError, IOError) as e:
        print("I/O error({0}): {1}".format(e.errno, e.strerror))
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise


ui.set_callbacks(send_func=send_file)

ui.show()
