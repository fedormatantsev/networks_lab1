from PyQt5.QtCore import QThread
from PyQt5.QtWidgets import *
import socket
import json
from os import path
import hashlib

BUFFER_SIZE = 1024

#   Progress window
class ProgressDialog:
    def __init__(self, caption):
        self.dialog = QDialog()
        self.dialog.setWindowTitle(caption)
        diag_layout = QHBoxLayout(self.dialog)
        self.progress_bar = QProgressBar()

        diag_layout.addWidget(self.progress_bar)
        self.dialog.show()

    def update_progress(self, value):
        self.progress_bar.setValue(value)

def sha256_checksum(filename, block_size=65536):
    sha256 = hashlib.sha256()
    with open(filename, 'rb') as f:
        for block in iter(lambda: f.read(block_size), b''):
            sha256.update(block)
    return sha256.hexdigest()

class FileRecieverThread(QThread):
    def __init__(self, ui, addr=('127.0.0.1', 8080), folder='.'):
        QThread.__init__(self)
        # dict{addr:{name, recieved, size, sha256, descriptor}}
        self.currently_recieving_file = dict()
        self.addr = addr
        self.folder = folder
        self.sock = None
        self.ui = ui
        try:
            self.sock = socket.socket(socket.AF_INET, # Internet
                                 socket.SOCK_DGRAM) # UDP
            self.sock.bind(self.addr)
        except OSError:
            self.ui.show_warning_msg('Address is already in use')
            return

    def __del__(self):
        self.wait()
        self.sock.close()

    def run(self):
        while True:
            data, addr = self.sock.recvfrom(BUFFER_SIZE)
            if addr not in self.currently_recieving_file:
                print ("received meta:", data.decode())
                meta = json.loads(data.decode())
                file_name = path.join(self.folder, meta['file_name'])
                self.ui.progress_diags_receive[file_name] = ProgressDialog("Receiving: " + file_name)
                self.ui.update_progress(file_name, 0)

                file = open(file_name, 'wb')
                self.currently_recieving_file[addr] = {'name':file_name,
                                                       'size':meta['size'],
                                                       'sha256':meta['sha256'],
                                                       'file':file,
                                                       'recieved':0}
            else:
                current_file = self.currently_recieving_file[addr]
                current_file['file'].write(data)
                current_file['recieved'] += len(data)
                self.ui.update_progress(current_file['name'], 100*current_file['recieved']//current_file['size'])
                if current_file['recieved'] >= current_file['size']:
                    print('file {0} was totally recieved ({1} bytes).'.format(current_file['name'], current_file['size']))
                    current_file['file'].close()
                    sha256 = sha256_checksum(current_file['name'])
                    # todo signal to callback
                    '''
                    if sha256 == current_file['sha256']:
                        self.ui.show_warning_msg('File {0} was recieved. Checksum correct.'.format(current_file['name']))
                    else:
                        self.ui.show_warning_msg('File {0} was recieved with errors. Checksum incorrect!'.format(current_file['name']))
                        '''
                    del self.currently_recieving_file[addr]
