import sys
from PyQt5.QtWidgets import *
from threading import Lock
from common import *
import re

class UI:
    def __init__(self):
        #   Send callback; Will be triggered on "send" button click
        self.send_func = None

        #   There seems to be no race conditions, but if one appears, use Locks
        self.send_func_lock = Lock()

        self.app = QApplication(sys.argv)

        #   Main window
        self.dialog = QDialog()

        v_layout = QVBoxLayout(self.dialog)

        prop_group = QGroupBox()
        prop_group_layout = QHBoxLayout()
        self.port_text_input = QLineEdit("127.0.0.1:8080")
        port_text_input_label = QLabel("Address:")
        prop_group.setLayout(prop_group_layout)
        prop_group_layout.addWidget(port_text_input_label)
        prop_group_layout.addWidget(self.port_text_input)

        v_layout.addWidget(prop_group)

        buttons_group = QGroupBox()
        buttons_group_layout = QHBoxLayout()
        buttons_group.setLayout(buttons_group_layout)

        self.start_server = QPushButton("Start server")
        self.start_server.clicked.connect(self.receive_file)
        select_button = QPushButton("Select file")
        select_button.clicked.connect(self.open_file)
        send_button = QPushButton("Send")
        send_button.clicked.connect(self.send_file)

        buttons_group_layout.addWidget(self.start_server)
        buttons_group_layout.addWidget(select_button)
        buttons_group_layout.addWidget(send_button)

        v_layout.addWidget(buttons_group)

        self.status_label = QLabel("No file selected")
        v_layout.addWidget(self.status_label)

        self.out_file = ""
        self.progress_diags_send = dict()
        self.progress_diags_receive = dict()

    def set_callbacks(self, send_func):
        self.send_func_lock.acquire()
        self.send_func = send_func
        self.send_func_lock.release()

    def show(self):
        self.dialog.show()
        return self.app.exec()

    def open_file(self):
        file = QFileDialog.getOpenFileName(parent=self.dialog, caption="Select a file to transfer")
        if file[0] and path.isfile(file[0]):
            self.out_file = file[0]
            self.status_label.setText("file: " + str(self.out_file))
        else:
            print('File "{0}" was not found'.format(file[0]), file=sys.stderr)

    #  don't call this one explicitly
    def send_file(self):
        self.send_func_lock.acquire()
        if self.send_func is not None:
            self.progress_diags_send[self.out_file] = ProgressDialog(caption="Sending: " + self.out_file)
            self.send_func(self.out_file)
        self.send_func_lock.release()

    def receive_file(self):
        self.start_server.setEnabled(False)
        folder = QFileDialog.getExistingDirectory(parent=self.dialog, caption='Choose a folder where to save files')
        if folder and path.isdir(folder) and path.exists(folder):
            self.status_label.setText("Recieving files to folder: {0}".format(folder))
            self.file_reciever = FileRecieverThread(self, addr=(str(socket.INADDR_ANY), 8080), folder=folder) # addr=self.get_address()
            self.file_reciever.start()
        else:
            print('Folder "{0}" does not exist'.format(folder), file=sys.stderr)

    def update_progress(self, file, value):
        receive_progress = self.progress_diags_send.get(file, None)
        send_progress = self.progress_diags_receive.get(file, None)

        if send_progress is not None and receive_progress is not None:
            # Note: you shouldn't get there. Proper file handling should be done within
            # "socket" logic
            self.show_warning_msg("Can't send and receive a file simultaneously.")
            return

        if receive_progress is not None:
            receive_progress.update_progress(value)

        if send_progress is not None:
            send_progress.update_progress(value)

    # return address tuple (ip, port)
    def get_address(self):
        pattern = "^(?P<ip>\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3})(:(?P<port>\d+))?$"
        match = re.match(pattern, self.port_text_input.text())
        if match == None:
            self.show_warning_msg("Incorrect address. Please enter ip and port like '127.0.0.1:8080'")
            return None
        else:
            ip_port = match.groupdict()
            address = ip_port['ip']
            if 'port' in ip_port:
                port = int(ip_port['port'])
            else:
                port = 8080
        return (address, int(port))

    # shows msg text in a new dialog pop up
    def show_warning_msg(self, msg):
        msg_box = QMessageBox()
        msg_box.setWindowTitle("Warning")
        msg_box.setText(msg)
        msg_box.exec()
